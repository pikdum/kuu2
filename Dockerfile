FROM clojure:lein-alpine
WORKDIR /app/
COPY project.clj .
RUN lein deps
COPY . .
RUN lein uberjar

FROM adoptopenjdk/openjdk8-openj9:alpine
MAINTAINER pikdum <pikdum@kuudere.moe>
WORKDIR /app/
EXPOSE 3000
#ENV JAVA_OPTS -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap
RUN apk add --no-cache curl

COPY --from=0 /app/target/uberjar/kuu2.jar /app/target/uberjar/
HEALTHCHECK CMD curl --fail http://localhost:3000/health || exit 1
CMD java $JAVA_OPTS -jar /app/target/uberjar/kuu2.jar