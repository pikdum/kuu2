-- :name get-posts :? :*
-- :doc retrieves all posts
SELECT * FROM posts
ORDER BY bumptime desc

-- :name get-thread :? :*
-- :doc retrives thread
SELECT * FROM posts
WHERE post_id = :post_id
OR parent = :post_id

-- :name create-post! :i! :raw
-- :doc create post
INSERT INTO posts
(subject, name, data, parent, timestamp, bumptime)
VALUES (:subject, :name, :data, :parent, :timestamp, :bumptime)

-- :name bump! :! :n
-- :doc bump a thread
UPDATE posts
SET bumptime=:bumptime
WHERE post_id=:parent

-- :name get-threads :? :*
-- :doc retrieves all posts
SELECT * FROM posts
WHERE parent=0
ORDER BY bumptime desc
