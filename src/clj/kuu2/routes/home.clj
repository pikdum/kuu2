(ns kuu2.routes.home
  (:require [kuu2.layout :as layout]
            [compojure.core :refer [defroutes GET POST]]
            [ring.util.http-response :as response]
            [selmer.filter-parser :as parser]
            [clojure.algo.generic.functor :refer [fmap]]
            [kuu2.db.core :as db]
            [clojure.string :as str]
            [clj-time.core :as t]
            [clj-time.format :as f]
            [clojure.java.io :as io]))

(defn clean [x]
  (-> x
      str/trim
      parser/escape-html))

(def date-formatter
  (f/formatter "yyyy/MM/dd HH:mm:ss"))

(defn get-posts []
  {:headers {"Content-Type" "application/json"}
   :body (db/get-posts)})

(defn get-threads []
  {:headers {"Content-Type" "application/json"}
   :body (db/get-threads)})

(defn get-thread [id]
  {:headers {"Content-Type" "application/json"}
   :body (db/get-thread {:post_id id})})

(defn create-post! [post]
  {:pre [(<= (count (post :name)) 30),
         (<= (count (post :subject)) 30),
         (<= (count (post :data)) 10000),
         (> (count (post :data)) 0),
         (if (= (post :parent) "0")
           (> (count (post :subject)) 0)
           true)]}

  (def date (f/unparse date-formatter (t/now)))
  (let [post_ (assoc post
                    :timestamp date
                    :bumptime date
                    :name (if (= (count (post :name)) 0)
                            "rei"
                            (post :name))
                    :subject (if (post :subject)
                               (post :subject)
                               ""))
        post__ (assoc post_
                      :post_id ((keyword "last_insert_rowid()") (db/create-post! post_)))]
    (if (not (= (post__ :parent) "0"))
      (db/bump! post__))
    {:headers {"Content-Type" "application/json"}
     :body post__}))

(defroutes home-routes
  (GET "/health" [] {:headers {"Content-Type" "application/json"}
                     :body {:status "success"}})
  (GET "/post/" [] (get-posts))
  (GET "/thread/" [] (get-threads))
  (GET "/thread/:id/" [id] (get-thread id))
  (POST "/thread/:parent/" [parent] (fn [req]
                              (let [post (fmap clean (req :params))]
                               (create-post! post)))))
