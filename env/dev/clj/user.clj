(ns user
  (:require [luminus-migrations.core :as migrations]
            [kuu2.config :refer [env]]
            [mount.core :as mount]
            kuu2.core))

(defn start []
  (mount/start-without #'kuu2.core/repl-server))

(defn stop []
  (mount/stop-except #'kuu2.core/repl-server))

(defn restart []
  (stop)
  (start))

(defn migrate []
  (migrations/migrate ["migrate"] (select-keys env [:database-url])))

(defn rollback []
  (migrations/migrate ["rollback"] (select-keys env [:database-url])))


